fetch("https://jsonplaceholder.typicode.com/posts")
    .then((response) => response.json())
    .then((result) => {
        console.log(result);
        return showPost(result);
    })
    .catch((err) => console.log(err));

const showPost = (posts) => {
    const entries = document.getElementById("div-post-entries");
    posts.forEach((post) => {
        entries.innerHTML += `
        <div id=${post.id}>
         <h3 id=post-title-${post.id}>${post.title}</h3>
         <p id=post-body-${post.id}>${post.body}</p>
         <button onclick="editPost(${post.id})">Edit</button>
         <button onclick="deletePost(${post.id})">Delete</button>
        </div>
        `;
    });
};

const addPostForm = document.querySelector("#form-add-post");
const editPostForm = document.querySelector("#form-edit-post");
const title = document.getElementById("txt-title");
const body = document.getElementById("txt-body");
const titleEdit = document.getElementById("txt-edit-title");
const bodyEdit = document.getElementById("txt-edit-body");
const submitEdit = document.getElementById("submit-edit");
const hidden = document.getElementById("txt-edit-id");

addPostForm.addEventListener("submit", (e) => {
    e.preventDefault();

    fetch("https://jsonplaceholder.typicode.com/posts", {
        method: "POST",
        body: JSON.stringify({
            title: title.value,
            body: body.value,
            userId: 1,
        }),
        headers: { "content-type": "application/json" },
    })
        .then((response) => response.json())
        .then((result) => {
            console.log(result);
            alert("Done adding new post!");
            title.value = "";
            body.value = "";
        });
});

const deletePost = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, { method: "DELETE" });
    document.getElementById(id).remove();
    alert("post has been deleted");
};

const editPost = (id) => {
    titleEdit.focus();
    alert("Your are editing post id: " + id);
    const thisTitle = document.getElementById(`post-title-${id}`);
    const thisBody = document.getElementById(`post-body-${id}`);
    hidden.value = id;
    titleEdit.value = thisTitle.innerText;
    bodyEdit.value = thisBody.innerText;
    submitEdit.disabled = false;

    editPostForm.addEventListener("submit", (e) => {
        e.preventDefault();
        submitEdit.disabled = true;
        fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
            method: "PUT",
            body: JSON.stringify({
                // id: id,
                title: titleEdit.value,
                body: bodyEdit.value,
                userId: 1,
            }),
            headers: { "content-type": "application/json; charset=UTF-8" },
        })
            .then((response) => response.json())
            .then((result) => {
                console.log(result);
                alert("Done updating!");
                bodyEdit.value = "";
                titleEdit.value = "";
            });
    });
};
